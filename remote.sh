#!/bin/sh

viewHelp () {
cat << EOF
# -----------------------------------------------------------------------------
# Executes the commands contained in scripts added to: ~/Dropbox/run and sends
# notification email on success / failure. Note that this script is called
# automatically by incrontab.
#
#   Usage:
#       $0 DIR FILE
#
#   Where:
#       FILE is a script to be executed
# -----------------------------------------------------------------------------
EOF
}

# Get script name
EXEC="$1/$2"

# Ignore temporary system files and directories
if [ $(echo "$EXEC" | sed -e '/^\./d' -e '/\/\./d' -e '/\/$/d') ]; then

	# Run script and capture output
	chmod +x "$EXEC"
	begin=$(date "+%F %R")
	MSG=$("$EXEC") || FAIL=1
	end=$(date "+%F %R")

	# Check result
	if [ "$FAIL" ]; then
		SUB="Subject: ADMIN: $2 fail"
	else
		SUB="Subject: ADMIN: $2 success"
	fi

	# Add start and finish times to notification and convert to HTML
	MSG="<pre style=\"font-family:'Courier New';\">Time started:  $begin\nTime finished: $end\n\n$MSG</pre>"

	# Send notification email (note 2 newlines before mesage body and full ssmtp path)
	EMAIL="peet.whittaker@gmail.com"
	HEAD="To: $EMAIL\nFrom: $EMAIL"
	TYPE="Content-Type: text/html"
	echo "$HEAD\n$SUB\n$TYPE\n\n$MSG" | /usr/sbin/ssmtp "$EMAIL"

	# Clean up
	rm "$EXEC"
fi
