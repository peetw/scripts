#!/usr/bin/php
<?php

/* This scipt runs as a crontab job every 30 minuts and checks to see whether
 * any new notifcations have been recieved.
 *
 * If a new notification is recieved, then the scripts sends out a warning email
 * to the contacts listed below.
 *
 * Currently checking
 *  - POS attacks
 *  - POS low fuel mails
 *  - War decs
 *  - Insufficient funds to pay a bill
 */


/* TO DO
 *  - Mail on db connection error, see:
 *    http://www.php.net/manual/en/function.mysql-error.php#68077
 *  - Get corp name for POS attackers and war declarers
 */


// Specify mail properties
//$KAZO = "peet.whittaker@gmail.com," .            // Halika
$KAZO = "miskundukt@gmail.com," .              // Misconduct
        "nejc.levstek@gmail.com," .            // i0Ni
        "olly@me.com," .                       // Yos
        "Havemann.martin@gmail.com," .         // Roger
        "info@37c.dk," .                       // RXOOR
        "knijpstra@hotmail.nl";               // excal
//$KAZO = "peet.whittaker@gmail.com";

$FICT = "peet.whittaker@gmail.com" .
        ", nejc.levstek@gmail.com";
//$FICT = "peet.whittaker@gmail.com";

// Specify Jabber properties (see ~./sendxmpprc for config)
$JABBER_ALIAS = "Skynet";
$JABBER_ROOM = "[kazo]@conference.eve-mail.net";

// Set ownerID for characters
$halika = 761079428;
$madprof = 90220832;

// Set up connection to eve_tools_API MySQL database
$username = "eve_tools_admin";
$password = "eve_tools_admin_pw";
$eveAPI = "yapeal";
$eveStaticDump = "eve_static_dump";

$db1 = mysql_connect("localhost", $username, $password);
$db2 = mysql_connect("localhost", $username, $password, true);

mysql_select_db($eveAPI, $db1) or die("Unable to select yapeal database");
mysql_select_db($eveStaticDump, $db2) or die("Unable to select EVE database");


// Get ownerID, notificationID, sentDate and typeID from database
$query = "SELECT ownerID, notificationID, sentDate, typeID FROM charNotifications";
$mainResult = mysql_query($query, $db1) or die (mysql_error());

// Determine number of rows in result
$num = mysql_numrows($mainResult);

if ($num == 0) {
    die("The charNotifications table contains no entries");
}


// Loop through results and check for new notifications since last check
$i = 0;
while ($i < $num) {
    // Check if notification is new
    $notificationID = mysql_result($mainResult, $i, "notificationID");
    $query = "SELECT notificationID FROM utilNotifications WHERE notificationID=".$notificationID;
    $processedResult = mysql_query($query, $db1) or die (mysql_error());
    $processed = mysql_numrows($processedResult);

    // Check whether notification text has been retrieved from API yet
    $query = "SELECT text FROM charNotificationTexts WHERE notificationID=".$notificationID;
    $textResult = mysql_query($query, $db1) or die (mysql_error());
    $textExist = mysql_numrows($textResult);

    if (!$processed && $textExist) {

        // Get notification's typeID, ownerID and sentDate
        $typeID = mysql_result($mainResult, $i, "typeID");
        $ownerID = mysql_result($mainResult, $i, "ownerID");
        $sentDate = mysql_result($mainResult, $i, "sentDate");

        // POS ATTACK
        if ($typeID == 75) {

            // Get notification text
            $text = mysql_result($textResult, 0, "text");
            $text = explode("\n", $text);

            // Parse text so it is human readable
            $allianceID = preg_replace("/aggressorAllianceID: /", "", $text[0]);
            $armor = preg_replace("/armorValue: /", "", $text[3]);
            $hull = preg_replace("/hullValue: /", "", $text[4]);
            $moonID = preg_replace("/moonID: /", "", $text[5]);
            $shield = preg_replace("/shieldValue: /", "", $text[6]);
            $structureID = preg_replace("/typeID: /", "", $text[8]);

            // Get name of attackers alliance
            $query = "SELECT name FROM eveAllianceList WHERE allianceID=".$allianceID;
            $allianceResult = mysql_query($query, $db1);
            $allianceName = mysql_fetch_array($allianceResult);

            // Cross reference location names from eveStaticDump
            $query = "SELECT itemName FROM invNames WHERE itemID=".$moonID;
            $moonResult = mysql_query($query, $db2);
            $moonName = mysql_result($moonResult, 0, "itemName");

            // Cross reference structure type from eveStaticDump
            $query = "SELECT typeName FROM invTypes WHERE typeID=".$structureID;
            $structureResult = mysql_query($query, $db2);
            $structType = mysql_result($structureResult, 0, "typeName");

            // Check notification owner
            if ($ownerID == $halika) {
                $subject = "EVE - KAZO POS Under Attack!";
                $recipients = $KAZO;
            } else if ($ownerID == $madprof) {
                $subject = "EVE - FICT POS Under Attack!";
                $recipients = $FICT;
            }

            // Generate message body
            $message = "\n\nTime of attack: ".$sentDate."\n\n" .
            "Attacker: ".$allianceName[0]."\n" .
            "Location: ".$moonName."\n" .
            "Structure: ".$structType."\n\n".
            "Shield HP: " . round(floatval($shield) * 100, 2) . "%\n" .
            "Armor HP: " . round(floatval($armor) * 100, 2) . "%\n" .
            "Hull HP: " . round(floatval($hull) * 100, 2) . "%\n\n" .
            "Quick, to the Batmobile Robin!";

            // Send mail
            mail($recipients, $subject, $message);

            // Send Jabber message
            if ($recipients == $KAZO) {
				$cmd = "echo \"\nKAZO POS UNDER ATTACK!".$message."\" | sendxmpp -r ".$JABBER_ALIAS." -c ".$JABBER_ROOM;
				exec($cmd);
			}
        }
        // POS LOW ON FUEL
        else if ($typeID == 76) {

            // Get notification text
            $text = mysql_result($textResult, 0, "text");
            $text = explode("\n", $text);

            // Cross-reference moon location from eveStaticDump
            $moonID = preg_replace("/moonID: /", "", $text[2]);
            $query = "SELECT itemName FROM invNames WHERE itemID=".$moonID;
            $moonResult = mysql_query($query, $db2);
            $moonName = mysql_result($moonResult, 0, "itemName");

            // Cross-reference POS type from eveStaticDump
            $posID = preg_replace("/typeID: /", "", $text[4]);
            $query = "SELECT typeName FROM invTypes WHERE typeID=".$posID;
            $posResult = mysql_query($query, $db2);
            $posType = mysql_result($posResult, 0, "typeName");

            // Check whether sov fuel bonus applies
            $allianceID = preg_replace("/allianceID: /", "", $text[0]);
            $solarSystemID = preg_replace("/solarSystemID: /", "", $text[3]);
            $query = "SELECT * FROM mapSovereignty WHERE `factionID`='0' AND `allianceID`='".$allianceID."' AND `solarSystemID`='".$solarSystemID."'";
            $sovResult = mysql_query($query, $db1);
            $sovBonus = (mysql_numrows($sovResult) ? 0.75 : 1.0);

            // Initialise message
            $message = "";

            if (count($text) < 10) {
                // Get remaining fuel block quantity
                $fuelBlockNum = preg_replace("/- quantity: /", "", $text[6]);

                // Cross-reference fuel block type from eveStaticDump
                $fuelBlockID = preg_replace("/  typeID: /", "", $text[7]);
                $query = "SELECT typeName FROM invTypes WHERE typeID=".$fuelBlockID;
                $fuelBlockResult = mysql_query($query, $db2);
                $fuelBlockType = mysql_result($fuelBlockResult, 0, "typeName");

                // Calculate remaining fuel time
                $fuelHrs = 0;
                if (preg_match("/Small/", $posType)) {
                    $fuelHrs = floor($fuelBlockNum / round(10 * $sovBonus));
                } else if (preg_match("/Medium/", $posType)) {
                    $fuelHrs = floor($fuelBlockNum / round(20 * $sovBonus));
                } else {
                    $fuelHrs = floor($fuelBlockNum / round(40 * $sovBonus));
                }

                // Generate message body
                $message = "\n\nTime: $sentDate\n\n" .
                "Location: $moonName\n" .
                "Structure: $posType\n" .
                "Fuel remaining: $fuelBlockNum $fuelBlockType ($fuelHrs hrs)\n\n" .
                "I'mma gonna sell all of our POS one day...";
            } else {
                // Get remaining fuel block quantity
                $fuelBlockNum = preg_replace("/- quantity: /", "", $text[8]);

                // Cross-reference fuel block type from eveStaticDump
                $fuelBlockID = preg_replace("/  typeID: /", "", $text[9]);
                $query = "SELECT typeName FROM invTypes WHERE typeID=".$fuelBlockID;
                $fuelBlockResult = mysql_query($query, $db2);
                $fuelBlockType = mysql_result($fuelBlockResult, 0, "typeName");

                // Get remaining charter quantity
                $fuelCharterNum = preg_replace("/- quantity: /", "", $text[6]);

                // Cross-reference charter type from eveStaticDump
                $fuelCharterID = preg_replace("/  typeID: /", "", $text[7]);
                $query = "SELECT typeName FROM invTypes WHERE typeID=".$fuelCharterID;
                $fuelCharterResult = mysql_query($query, $db2);
                $fuelCharterType = mysql_result($fuelCharterResult, 0, "typeName");

                // Calculate remaining fuel time
                if (preg_match("/Small/", $posType)) {
                    $fuelHrs = floor($fuelBlockNum / round(10 * $sovBonus));
                } else if (preg_match("/Medium/", $posType)) {
                    $fuelHrs = floor($fuelBlockNum / round(20 * $sovBonus));
                } else {
                    $fuelHrs = floor($fuelBlockNum / round(40 * $sovBonus));
                }

                $fuelHrs = ($fuelCharterNum < $fuelHrs ? $fuelCharterNum : $fuelHrs);

                // Generate message body
                $message = "\n\nTime: $sentDate\n\n" .
                "Location: $moonName\n" .
                "Structure: $posType\n" .
                "Fuel remaining: $fuelBlockNum $fuelBlockType and " .
                "$fuelCharterNum $fuelCharterType ($fuelHrs hrs)\n\n" .
                "I'mma gonna sell all of our POS one day...";
            }

            // Check notification owner
            if ($ownerID == $halika) {
                $subject = "EVE - KAZO POS Low On Resources!";
                $recipients = $KAZO;
            } else if ($ownerID == $madprof) {
                $subject = "EVE - FICT POS Low On Resources!";
                $recipients = $FICT;
            }

            // Send mail
            mail($recipients, $subject, $message);

            // Send Jabber message
            if ($recipients == $KAZO) {
				$cmd = "echo \"\nKAZO POS LOW ON FUEL!".$message."\" | sendxmpp -r ".$JABBER_ALIAS." -c ".$JABBER_ROOM;
				exec($cmd);
			}

        }
        // INSUFFICIENT FUNDS TO PAY UPCOMING BILL
        else if ($typeID == 11) {

            // Get notification text
            $text = mysql_result($textResult, 0, "text");
            $text = explode("\n", $text);

            // Get due date and convert from Microsoft epoch time
            $dueDate = preg_replace("/dueDate: /", "", $text[1]);
            $dueDate = ($dueDate / 10000000) - 11644473600;
            $dueDate = date("Y-m-d H:i:s",  $dueDate);

            // Generate message body
            $message = "\n\nThere are insufficient funds to pay the bill " .
            "due on: $dueDate\n\n" .
            "Time of notification: $sentDate";

            // Check notification owner
            if ($ownerID == $halika) {
                $subject = "EVE - KAZO Insufficient Funds For Upcoming Bill!";
                $recipients = $KAZO;
            } else if ($ownerID == $madprof) {
                $subject = "EVE - FICT Insufficient Funds For Upcoming Bill!";
                $recipients = $FICT;
            }

            // Send mail
            mail($recipients, $subject, $message);
        }
        // WAR DEC
        else if ($typeID == 5 || $typeID == 27) {

            // Get notification text
            $text = mysql_result($textResult, 0, "text");
            $text = explode("\n", $text);

            // Get alliance name of aggressor
            $allianceID = preg_replace("/declaredByID: /", "", $text[2]);
            $query = "SELECT name FROM eveAllianceList WHERE allianceID=".$allianceID;
            $allianceResult = mysql_query($query, $db1);
            $allianceName = mysql_fetch_array($allianceResult);

            // Generate message body
            $message = "\n\nTime of declaration: $sentDate\n\n" .
            "Declared by: $allianceName[0]\n";

            // Check notification owner
            if ($ownerID == $halika) {
                $subject = "EVE - KAZO War Dec!";
                $recipients = $KAZO;
            } else if ($ownerID == $madprof) {
                $subject = "EVE - FICT War Dec!";
                $recipients = $FICT;
            }

            // Send mail
            mail($recipients, $subject, $message);
        }

        // Mark notification as processed
        $query = "INSERT INTO utilNotifications (ownerID, notificationID, sentDate, typeID) VALUES ('$ownerID', '$notificationID', '$sentDate', '$typeID')";
        mysql_query($query, $db1);
    }
$i++;
}

// Close connections to MySQL
mysql_close($db1);
mysql_close($db2);
?>
