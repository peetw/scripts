#!/usr/bin/php
<?php
// Convert price file to correct format
exec("sed -e 's/<\/td>/ /g' -e 's/<\/tr>//g' -e 's/<[a-z][a-z]>//g' -e 's/<[a-z][a-z][a-z][a-z]*>//g' -e 's/<\/[a-z][a-z][a-z][a-z]*>//g' -e '/Components/d' -e '/Material/d' -e 's/.$//' -e '/^$/d' /home/peet/prices.html > /home/peet/prices.dat");

// Get current date
$date = gmdate("Y-m-d");

// Set up connections to each database
$username = "eve_tools_admin";
$password = "eve_tools_admin_pw";
$wwwDB = "eve_tools";
$eveStatic = "eve_static_dump";

$db1 = mysql_connect("localhost", $username, $password, false, 128); // 'false,128' added to allow "LOAD DATA LOCAL INFILE", see last comment: https://bugs.php.net/bug.php?id=55737&edit=3
$db2 = mysql_connect("localhost", $username, $password, true);

mysql_select_db($wwwDB, $db1) or die("Unable to select eve_tools database\n");
mysql_select_db($eveStatic, $db2) or die("Unable to select eve_static_dump database\n");


// Load data from file
$query = "LOAD DATA LOCAL INFILE '/home/peet/prices.dat' INTO TABLE t2_comps FIELDS TERMINATED BY ' ' LINES TERMINATED BY '\n' (item, jita_price) SET `date`='".$date."'";
$result = mysql_query($query, $db1);
if (!$result) {
	die("Could not load data into table:\n\n".mysql_error($db1)."\n\n");
}

// Determine number of rows in table
$query = "SELECT item FROM t2_comps WHERE date='".$date."'";
$result = mysql_query($query, $db1);
$num = mysql_numrows($result);

if ($num != 47) {
       die("The table contains the wrong amount of entries\n");
}

// Loop through components and calculate manufacture cost
$i = 11;
while ($i < $num) {
	// Get item name
	$itemName = preg_replace("/_/", " ", mysql_result($result, $i, "item"));

	// Get typeID for item
	$query = "SELECT typeID FROM invTypes WHERE typeName='".$itemName."'";
	$typeIDResult = mysql_query($query, $db2);
	$itemTypeID = mysql_result($typeIDResult, 0, "typeID");

	// Get number of required materials for item
	$query = "SELECT materialTypeID, quantity FROM invTypeMaterials WHERE typeID='".$itemTypeID."'";
	$matsResult = mysql_query($query, $db2);
	$numMats = mysql_numrows($matsResult);

	// Loop through materials and determine total cost
	$j = 0;
	$totalCost = 0.0;
	while ($j < $numMats) {
		// Get typeID
		$matTypeID = mysql_result($matsResult, $j, "materialTypeID");

		// Get name
		$query = "SELECT typeName FROM invTypes WHERE typeID='".$matTypeID."'";
		$matNameResult = mysql_query($query, $db2);
		$matName = preg_replace("/ /", "_", mysql_result($matNameResult, 0, "typeName"));

		// Get price
		$query = "SELECT jita_price FROM t2_comps WHERE item='".$matName."' AND date='".$date."'";
		$priceResult = mysql_query($query, $db1);
		$price = mysql_result($priceResult, 0, "jita_price");

		// Get quantity
		$quantity = mysql_result($matsResult, $j, "quantity");

		// Calculate cost
		$totalCost = $totalCost + ($quantity * $price);

		$j++;
	}

	// Add manufacture price to table
	$itemName = mysql_result($result, $i, "item");
	$query = "UPDATE t2_comps SET manufacture_cost='".$totalCost."' WHERE item='".$itemName."' AND date='".$date."'";
	$updateResult = mysql_query($query, $db1);

$i++;
}

// Close connections to MySQL
mysql_close($db1);
mysql_close($db2);

// Delete price files
exec("rm -f /home/peet/prices.*");
?>
