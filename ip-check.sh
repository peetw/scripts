#!/bin/sh

# Get current external IP address from internet
IP_EXT=$(curl --silent http://ipecho.net/plain)

# Read cached IP from file
CACHE_FILE="/home/peet/scripts/.ip-cache"
IP_CACHED="0"

if [ -r "$CACHE_FILE" ]; then
	read IP_CACHED < "$CACHE_FILE"
fi

# Check if IP address has changed; if yes, alert over email and update cache file
if [ "$IP_EXT" != "$IP_CACHED"  ] && [ "$IP_EXT" != "" ]; then
	echo "$IP_EXT" > "$CACHE_FILE"
	echo "\nIP was: $IP_CACHED\nIP now: $IP_EXT" | mail -s "SERVER - IP address changed" peet.whittaker@gmail.com
fi


##################################################################
# THIS FILE HAS NOW BEEN SUPERSEDED BY NOIP2 UPDATER TOOL
#################################################################

See link for installation instructions:
http://support.no-ip.com/customer/portal/articles/363247-installing-the-linux-dynamic-update-client-on-ubuntu

Also note that to get it to run at boot, you need to add the following script to /etc/init.d/noip2

	#! /bin/sh

	### BEGIN INIT INFO
	# Provides:          noip2
	# Required-Start:    $remote_fs $syslog
	# Required-Stop:     $remote_fs $syslog
	# Should-Start:      $named
	# Default-Start:     2 3 4 5
	# Default-Stop:      0 1 6
	# Short-Description: Updates DNS hosted IP address
	# Description:       noip2 checks if external-facing IP address matches value
	#                    stored in no-ip.org DNS server and updates if req.
	### END INIT INFO

	#######################################################
	case "$1" in
	  start)
		echo "Starting noip2."
		/usr/local/bin/noip2
	  ;;
	  stop)
		echo -n "Shutting down noip2."
		kill $(pidof noip2)
	#   killproc -TERM /usr/local/bin/noip2
	  ;;
	  *)
		echo "Usage: $0 {start|stop}"
		exit 1
	esac

	exit 0
	#######################################################

Then run:
	update-rc.d noip2 defaults

This will create symlinks in all the /etc/rcX.d/ directories and the app will start automatically.
