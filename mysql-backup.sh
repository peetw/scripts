#!/bin/sh

# -----------------------------------------------------------------------------------------
# RUN AS A CRON JOB TO BACKUP MYSQL DATABASES
# -----------------------------------------------------------------------------------------

# Backup directory and current date
DIR_BAK="/home/peet/backup/mysql"
DATE=$(date -I)

# Backup each database in turn
mysql -s -r -u backup -pbackup_pw -e "SHOW DATABASES" | grep -Ev "(Database|information_schema|performance_schema)" | \
while read db; do \
mysqldump --single-transaction --flush-privileges -u backup -pbackup_pw $db -r $DIR_BAK/${db}_$DATE.sql; \
done

# Compress and remove individual backups
cd $DIR_BAK
tar -czvf $DATE.tar.gz *_$DATE.sql
rm *_$DATE.sql
